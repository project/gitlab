<?php
/**
 * @file
 * Code for the GitLab feature.
 */

include_once 'gitlab.features.inc';

define('GITLAB_API_REQUEST_OK', '200');
define('GITLAB_API_REQUEST_CREATED', '201');
define('GITLAB_API_REQUEST_BAD_REQUEST', '400');
define('GITLAB_API_REQUEST_UNAUTHORIZED', '401');
define('GITLAB_API_REQUEST_FORBIDDEN', '403');
define('GITLAB_API_REQUEST_NOT_FOUND', '404');
define('GITLAB_API_REQUEST_METHOD_NOT_ALLOWED', '405');
define('GITLAB_API_REQUEST_CONFLICT', '409');
define('GITLAB_API_REQUEST_SERVER_ERROR', '500');

function gitlab_request_error($code) {
  $msg = '';
  switch ($code) {
    case GITLAB_API_REQUEST_BAD_REQUEST:
      $msg = 'A required attribute of the API request is missing';
      break;
    case GITLAB_API_REQUEST_UNAUTHORIZED:
      $msg = 'The user is not authenticated, a valid user token is necessary';
      break;
    case GITLAB_API_REQUEST_FORBIDDEN:
      $msg = 'The request is not allowed';
      break;
    case GITLAB_API_REQUEST_NOT_FOUND:
      $msg = 'A resource could not be accessed';
      break;
    case GITLAB_API_REQUEST_METHOD_NOT_ALLOWED:
      $msg = 'The request is not supported';
      break;
    case GITLAB_API_REQUEST_CONFLICT:
      $msg = 'A conflicting resource already exists';
      break;
    case GITLAB_API_REQUEST_SERVER_ERROR:
      $msg = 'While handling the request something went wrong on the server side';
      break;
  }
  return $msg;
}

/**
 * Apply options to the wsconfig object
 *
 * @param object $wsconfig [reference]
 *  WSConfig object
 * @param array $options [reference]
 *  Options to pass into the object
 */
function gitlab_set_wsconfig_options(&$wsconfig, &$options) {
  // Apply any customizations passed through as options
  if (!empty($options['wsdata'])) {
    if (!empty($options['wsdata']['endpoint'])) {
      $wsconfig->setEndpoint($options['wsdata']['endpoint']);
    }
  }
}

/**
 * Build array of wsconfig call options
 *
 * @param string $private_token
 *  User's private token to authenticate the call
 * @param array $options
 *  Arbitrary set of options to pass to the wsconfig object.
 *  Ex: error handling callbacks for RESTClient.
 * @param integer $per_page [optional]
 *  Results per page (if the request supports paging)
 * @param integer $page [optional]
 *  Result page number
 * @return array
 *  Returns the array of options.
 */
function gitlab_set_wsconfig_call_options($private_token, $options, $per_page = NULL, $page = NULL) {
  $call_opts = array(
    'headers' => array(
      'Content-Type' => 'application/json',
      'PRIVATE-TOKEN' => $private_token,
      'Authorization' => 'Bearer ' . $private_token,
      'User-Agent' => 'Dropfort.com',
      ''
    ),
  );
  if (isset($page)) {
    $call_opts['query']['page'] = $page;
  }
  if (isset($per_page)) {
    $call_opts['query']['per_page'] = $per_page;
  }
  if (!empty($options['error_handling'])) {
    $call_opts['error_handling'] = $options['error_handling'];
  }
  return $call_opts;
}

/**
 * Constructs and executes a GitLab API call using WSData, and returns
 * the result as an array.
 * 
 * @param string $wsconfig_name
 *  The machine readable name of of the wsconfig entity describing the API request URI
 * @param string $method
 *  The wsconfig request method to execute
 * @param array $wsparams
 *  An array of replacement parameters for the wsconfig URI
 * @param array $wsbody
 *  An array of parameters which will be converted to 
 * @param string $private_token
 *  User's private token to authenticate the call
 * @param boolean $paginate (optional, default FALSE)
 *  A boolean indicating whether or not to paginate the request
 * @param integer $per_page (optional, default 100)
 *  The number of results to include on each request page
 * @param integer $page (optional, default 1)
 *  The number of the first page to request
 */
function gitlab_api_call($wsconfig_name, $method, $options, $wsparams, $wsbody, $private_token, $paginate = FALSE, $per_page = 100, $page = 1) {
  // Load the wsconfig
  $wsconfig = wsconfig_load_by_name($wsconfig_name);

  // Apply options
  gitlab_set_wsconfig_options($wsconfig, $options);

  $processor = new gitlabWsdataProcessor();

  $json_body = !empty($wsbody) ? drupal_json_encode($wsbody) : '';

  $wsconfig_options = ($paginate) ? 
    gitlab_set_wsconfig_call_options($private_token, $options, $per_page, $page) :
    gitlab_set_wsconfig_call_options($private_token, $options);

  // Check for a valid connector
  if ($wsconfig->connector) {
    $data = $wsconfig->call($method, $wsparams, $json_body, $wsconfig_options);

    if (FALSE !== $data) {
      if ($paginate) {
        // handle pagination to retrieve all repositories
        $response_data = $processor->process_result($data);
        $paginated_data = array('data' => array());
        while(isset($response_data['data']) and !empty($response_data['data'])) {
          $paginated_data = array('data' => array_merge($paginated_data['data'], $response_data['data']));
          $page += 1;
          $data = $wsconfig->call($method, $wsparams, $json_body,
            gitlab_set_wsconfig_call_options($private_token, $options, $per_page, $page)
          );
          $response_data = $processor->process_result($data);
        }
        return $paginated_data;
      } else {
        // no pagination, just process and return the results
        return $processor->process_result($data);
      }
    }
    else {
      watchdog('gitlab',
               'Failed to make GitLab API wsdata call for wsconfig @wsconfig_name with method @method',
               array('@wsconfig_name' => $wsconfig_name, '@method' => $method),
               WATCHDOG_ERROR);
    }
  }
}
