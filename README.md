GitLab
======

API support for GitLab 7.x

Latest API version supported is 7.2.x

Newer APIs will be added as they become available. Patches are always welcome.