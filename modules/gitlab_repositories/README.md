GitLab Repositories
===================

Includes API access to the Repositories and Repository Files in GitLab.


Requirements
-------------

Requires GitLab 6.7 or higher.