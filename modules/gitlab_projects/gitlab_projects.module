<?php

/**
 * @file
 * Project integration with GitLab
 */

include_once 'gitlab_projects.features.inc';

// Entity type for projects
define('GITLAB_PROJECTS_ENTITY_TYPE', 'project');
define('GITLAB_PROJECT_PROVIDER', 'gitlab');

/**
 * Implements hook_permission().
 */
function gitlab_projects_permission() {
  return array(
    'administer gitlab_projects' =>  array(
      'title' => t('Administer GitLab Projects'),
      'description' => t('Perform administration tasks GitLab Projects.'),
    ),
    'create own gitlab_projects' => array(
      'title' => t('Create GitLab Projects'),
      'description' => t('Create an equivalent project in GitLab.'),
    ),
    'edit own gitlab_projects' => array(
      'title' => t('Edit own GitLab Projects'),
      'description' => t('Edit own equivalent project in GitLab.'),
    ),
    'delete own gitlab_projects' => array(
      'title' => t('Delete own GitLab Projects'),
      'description' => t('Create an equivalent project in GitLab.'),
    ),
  );
  // @todo extend permissions to group projects
}

/**
 * Returns repo/project name.
 */
function gitlab_projects_project_name($project_json) {
  return $project_json['path_with_namespace'];
}

//========== Projects ==========//

/**
 *  Projects index
 *
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 */
function gitlab_projects_index($private_token, $options = array()) {
  if (!empty($private_token) ) {
    return gitlab_api_call('gitlab_projects', 'index', $options, array(), NULL, $private_token, TRUE);
  }

  return FALSE;
}


/**
 * Load a project
 *
 * @param string $pid
 *  Project ID
 * @param string $private_token
 *  The user's private token for connecting to this endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 */
function gitlab_projects_read_project($pid, $private_token, $options = array()) {
  // @todo check project access
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_projects', 'read', $options, array('%id' => $pid), NULL, $private_token);
  }
  return FALSE;
}

/**
 * Create a project
 *
 * @param array $data
 *  Data to update on the project
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 */
function gitlab_projects_create_project($data, $private_token, $options = array()) {
  // @todo check project access
  //if (gitlab_projects_access('create', '')) {
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_projects', 'create', $options, array(), $data, $private_token);
  }
  return FALSE;
}

/**
 * Create a project for a user
 *
 * @param array $data
 *  Data to update on the project
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 */
function gitlab_projects_create_user_project($user_id, $data, $private_token, $options = array()) {
  // @todo check project access
  //if (gitlab_projects_access('create', '')) {
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_user_projects', 'create', $options, array('%user_id' => $user_id), $data, $private_token);
  }
  return FALSE;
}

/**
 * Update a project
 *
 * @param string $pid
 *  Project ID
 * @param array $data
 *  Data to update on the project
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 */
function gitlab_projects_update_project($pid, $data, $private_token, $options = array()) {
  // @todo check project access
  //if (gitlab_projects_access('edit', $pid)) {
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_projects', 'update', $options, array('%id' => $pid), $data, $private_token);
  }
  return FALSE;
}

/**
 * Delete a project
 *
 * @param string $pid
 *  Project ID
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 */
function gitlab_projects_delete_project($pid, $private_token, $options = array()) {
  //if (gitlab_projects_access('delete', $pid)) {
  // @todo check access for project delete
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_projects', 'delete', $options, array('%id' => $pid), NULL, $private_token);
  }
  return FALSE;
}

//========== Project Team Members ==========//

/**
 * Load all project team members
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Return list of project team members, FALSE otherwise.
 */
function gitlab_projects_team_members_index($pid, $private_token, $options = array()) {
  // @todo check access for team members index
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_team_members', 'index', $options, array('%id' => $pid), NULL, $private_token, TRUE);
  }
  return FALSE;
}

/**
 * Load single project team member
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param mixed $gituid
 *  GitLab User ID
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Return information about requested team member, FALSE otherwise.
 */
function gitlab_projects_get_team_member($pid, $gituid, $private_token, $options = array()) {
  // @todo check access for project team members
  if (!empty($private_token)) {
   return gitlab_api_call('gitlab_project_team_members', 'read', $options, array('%id' => $pid, '%uid' => $gituid), NULL, $private_token);
  }
  return FALSE;
}

/**
 * Add single project team member
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param mixed $gituid
 *  GitLab Project ID
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Return information about requested team member, FALSE otherwise.
 */
function gitlab_projects_add_team_member($pid, $gituid, $private_token, $options = array()) {
  // @todo check access for adding project team members
  if (!empty($private_token)) {
    $parameters = array(
      'id' => $pid,
      'user_id' => $gituid,
      'access_level' => '10'
    );
    return gitlab_api_call('gitlab_project_team_members', 'create', $options, array('%id' => $pid), $parameters, $private_token);
  }
  return FALSE;
}

/**
 * Edit single project team member
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $gituid
 *  GitLab user ID
 * @param array $data
 *  Data to update team member
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @return array
 *  Return information about requested team member, FALSE otherwise.
 */
function gitlab_projects_update_team_member($pid, $gituid, $data, $private_token, $options = array()) {
  // @todo check access for editing project team members
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_team_members', 'update', $options, array('%id' => $pid, '%uid' => $gituid), $data, $private_token);
  }
  return FALSE;
}

/**
 * Delete single project team member
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param mixed $gituid
 *  GitLab user ID
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Return information about requested team member, FALSE otherwise.
 */
function gitlab_projects_delete_team_member($pid, $gituid, $private_token, $options = array()) {
  // @todo check access for deleting project team members
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_team_members', 'delete', $options, array('%id' => $pid, '%uid' => $gituid), NULL, $private_token);
  }
  return FALSE;
}

//========== Project Hooks ==========//

/**
 * Load all project hooks
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Return list of project hooks, FALSE otherwise.
 */
function gitlab_projects_hooks($pid, $private_token, $options = array()) {
  // @todo check access for hook index
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_hooks', 'index', $options, array('%id' => $pid), NULL, $private_token, TRUE);
  }
  return FALSE;
}

/**
 * Load single project hook
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param mixed $hook_id
 *  GitLab hook ID
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Return an array of information about requested hook, FALSE otherwise.
 */
function gitlab_projects_get_hook($pid, $hook_id, $private_token, $options = array()) {
  // @todo check access for project hooks
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_hooks', 'read', $options, array('%id' => $pid, '%hook_id' => $hook_id), NULL, $private_token);
  }
  return FALSE;
}

/**
 * Create project hook
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $hook_data
 *  Data to pass to the web service call, includes URL to the web service/hook endpoint
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @return array
 *  Returns an array of information about the created hook, FALSE otherwise.
 */
function gitlab_projects_create_hook($pid, $hook_data, $private_token, $options = array()) {
  // @todo check access for creating hooks
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_hooks', 'create', $options, array('%id' => $pid), $hook_data, $private_token);
  }
  return FALSE;
}

/**
 * Edit project hook
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param mixed $hook_id
 *  GitLab hook ID
 * @param string $hook_data
 *  Data to pass to the web service call, includes URL to the web service/hook endpoint
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @return array
 *  Returns an array of information about the edited hook, FALSE otherwise.
 */
function gitlab_projects_edit_hook($pid, $hook_id, $hook_data, $private_token, $options = array()) {
  // @todo check access for editing hooks
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_hooks', 'update', $options, array('%id' => $pid, '%hook_id' => $hook_id), $hook_data, $private_token);
  }
  return FALSE;
}

/**
 * Delete project hook
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param mixed $hook_id
 *  GitLab hook ID
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Returns an array of information about the deleted hook, FALSE otherwise.
 */
function gitlab_projects_delete_hook($pid, $hook_id, $private_token, $options = array()) {
  // @todo check access for project hooks
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_hooks', 'delete', $options, array('%id' => $pid, '%hook_id' => $hook_id), NULL, $private_token);
  }
  return FALSE;
}

//========== Project Branches ==========//

/**
 * Load all project branches
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Returns an array of branches, FALSE otherwise.
 */
function gitlab_projects_branches_index($pid, $private_token, $options = array()) {
  // @todo check access for project branch index
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_branches', 'index', $options, array('%id' => $pid), NULL, $private_token);
    // @todo - pagination seems to break this request... why?
  }
  return FALSE;
}

/**
 * Load single project branch
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $branch
 *  Name of branch
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Returns an array of information about the specified branch, FALSE otherwise.
 */
function gitlab_projects_get_branch($pid, $branch, $private_token, $options = array()) {
  // @todo check project access
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_branches', 'read', $options, array('%id' => $pid, '%branch' => $branch), NULL, $private_token);
  }
  return FALSE;
}

/**
 * Create project branch
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $branch_data
 *  Data to pass to the web service call:
 *    {"id":"<project id>", "branch_name": "<name of the branch>", "ref": "<commit sha or existing branch>"
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Returns an array of information about the created hook, FALSE otherwise.
 */
function gitlab_projects_create_branch($pid, $branch_data, $private_token, $options = array()) {
  // @todo check access for creating hooks
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_branches', 'create', $options, array('%id' => $pid), $branch_data, $private_token);
  }
  return FALSE;
}

/**
 * Protect project branch
 *
 * @param mixed $project
 *  Project object or GitLab Project ID
 * @param string $branch
 *  Name of branch
 * @return array
 *  Returns an array of information about the protected branch, FALSE otherwise.
 */
function gitlab_projects_branch_protect($project, $branch) {
  // @todo
  return FALSE;
}

/**
 * Unprotect project branch
 *
 * @param mixed $project
 *  Project object or GitLab Project ID
 * @param string $branch
 *  Name of branch
 * @return array
 *  Returns an array of information about the unprotected branch, FALSE otherwise.
 */
function gitlab_projects_branch_unprotect($project, $branch) {
  // @todo
  return FALSE;
}

//========== Project Tags ==========//

/**
 * Load all project tags
 *
 * @param mixed $project
 *  Project object or GitLab Project ID
 * @return array
 *  Returns an array of tags, FALSE otherwise.
 */
function gitlab_projects_tags($pid, $private_token, $options = array()) {
  // @todo check access for creating hooks
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_tags', 'read', $options, array('%id' => $pid), NULL, $private_token);
  }
  return FALSE;
}

//========== Project Commits ==========//

/**
 * Load all project commits
 *
 * @param mixed $project
 *  Project object or GitLab Project ID
 * @return array
 *  Returns an array of commits, FALSE otherwise.
 */
function gitlab_projects_commits($project) {
  // @todo
  return FALSE;
}

//========== Project Deploy Keys ==========//

/**
 * Load all project deploy keys
 *
 * @param mixed $project
 *  Project object or GitLab Project ID
 * @return array
 *  Returns an array of keys, FALSE otherwise.
 */
function gitlab_projects_deploy_keys_index($pid, $private_token, $options = array()) {
  // @todo
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_deploy_keys', 'index', $options, array('%id' => $pid), NULL, $private_token);
  }
  return FALSE;
}

/**
 * Load single project deploy key
 *
 * @param mixed $project
 *  Project object or GitLab Project ID
 * @param integer $key_id
 *  Deploy key id
 * @return array
 *  Returns an array of information about a single key, FALSE otherwise.
 */
function gitlab_projects_get_deploy_key($project, $key_id) {
  // @todo
  return FALSE;
}

/**
 * Create a project deploy key
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $key_data
 *  Data to pass to the web service call, includes project id, key title and key contents
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @return array
 *  Returns an array of information about the created hook, FALSE otherwise.
 */
function gitlab_projects_create_deploy_key($pid, $key_data, $private_token, $options = array()) {
  // @todo check access for creating hooks
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_deploy_keys', 'create', $options, array('%id' => $pid), $key_data, $private_token);
  }
  return FALSE;
}


/**
 * Delete a project deploy key
 *
 * @param mixed $project
 *  Project object or GitLab Project ID
 * @param integer $key_id
 *  Deploy key id
 * @return array
 *  Returns an array of information about the deleted key, FALSE otherwise.
 */
function gitlab_projects_delete_deploy_key($pid, $key_id, $private_token, $options = array()) {
  // @todo
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_deploy_keys', 'delete', $options, array('%id' => $pid, '%key_id' => $key_id), NULL, $private_token);
  }
  return FALSE;
}

//========== Project Events ==========//

/**
 * Get events for a single project
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Returns an array of information about the events for the specified project, FALSE otherwise.
 */
function gitlab_projects_get_events($pid, $private_token, $options = array()) {
  // @todo check project access
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_events', 'read', $options, array('%id' => $pid), NULL, $private_token);
  }
  return FALSE;
}

//========== Project Variables ==========//

/**
 * Load all project variables
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Returns an array of information about the variables for the specified project, FALSE otherwise.
 */
function gitlab_projects_variables_index($pid, $private_token, $options) {
  // @todo check project access
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_variables', 'index', $options, array('%id' => $pid), NULL, $private_token);
  }
  return FALSE;
}

/**
 * Load single project variable
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $key
 *  Machine name of the variable.
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Returns an array of information about the variables for the specified project, FALSE otherwise.
 */
function gitlab_projects_get_variable($pid, $key, $private_token, $options) {
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_variables', 'read', $options, array('%id' => $pid, '%key' => $key), NULL, $private_token);
  }
  return FALSE;
}

/**
 * Create single project variable
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $key
 *  Machine name of the variable.
 * @param string $value
 *  Value of the variable.
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Returns an array of information about the variables for the specified project, FALSE otherwise.
 */
function gitlab_projects_create_variable($pid, $key, $value, $private_token, $options) {
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_variables', 'create', $options, array('%id' => $pid), array('key' => $key, 'value' => $value), $private_token);
  }
  return FALSE;
}

/**
 * Update single project variable
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $key
 *  Machine name of the variable.
 * @param string $value
 *  Value of the variable.
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Returns an array of information about the variables for the specified project, FALSE otherwise.
 */
function gitlab_projects_update_variable($pid, $key, $value, $private_token, $options) {
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_variables', 'update', $options, array('%id' => $pid, '%key' => $key), array('value' => $value), $private_token);
  }
  return FALSE;
}

/**
 * Delete single project variable
 *
 * @param mixed $pid
 *  GitLab Project ID
 * @param string $key
 *  Machine name of the variable.
 * @param string $value
 *  Value of the variable.
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options (i.e. url)
 * @return array
 *  Returns an array of information about the variables for the specified project, FALSE otherwise.
 */
function gitlab_projects_delete_variable($pid, $key, $private_token, $options) {
  if (!empty($private_token)) {
    return gitlab_api_call('gitlab_project_variables', 'delete', $options, array('%id' => $pid, '%key' => $key), NULL, $private_token);
  }
  return FALSE;
}

//========== Other ==========//

/**
 * Access callback for GitLab projects
 *
 * @param string $action
 *  Action being performed
 *   - create, view, edit, delete
 * @param object $project
 *  Project entity
 * @param object $account [optional]
 *  User account object
 * @return boolean
 *  Returns TRUE if access is granted, FALSE otherwise.
 */
function gitlab_projects_access($action, $project, $account = NULL) {
  if (is_null($account)) {
    global $user;
    $account = $user;
  }

  if (!empty($account->gitlab_id)) {
    // @todo check permissions against project object
    // @todo check role permissions
    return TRUE;
  }

  return FALSE;
}
