<?php

/**
 * @file
 * Callback functions
 */

/**
 * Create a GitLab group
 *
 * @see gitlab_groups_create_group().
 */
function _gitlab_groups_create_group(&$name, &$path, &$account) {
  // @todo  
}

/**
 * List all groups for the given user
 *
 * @see gitlab_groups_groups().
 */
function _gitlab_groups_groups(&$account) {
  // @todo
}

/**
 * List a single group for the given user
 *
 * @see gitlab_groups_get_group().
 */
function _gitlab_groups_get_group(&$id, &$account) {
  // @todo
}

/**
 * Transfer a project to the given group for the given user
 *
 * @see gitlab_groups_transfer_group().
 */
function _gitlab_groups_transfer_group(&$group_id, &$project_id, &$account) {
  // @todo
}